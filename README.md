# Titulo del proyecto

_Descripción del proyecto explicando para que vale_.


## Comandos disponibles

In la carpeta del proyecto, puedes ejecutar:

### `yarn start`

Ejecuta la aplicación en el modo de desarrollo. \
Abre [http://localhost:3000](http://localhost:3000) para verlo en el navegador.

La página se recargará si realiza modificaciones.\ 
También verá errores de pelusa en la consola.

### `yarn test`

Lanza los tests en watch mode.
Mas información en [jestjs](https://jestjs.io/docs/getting-started)

### `yarn build`

Construye la aplicación para producción en la carpeta `build`. \ 

### `yarn eject`

**Nota: este comando es de una sola dirección, no podras deshacer.*

Si no está satisfecho con la herramienta de compilación y las opciones de configuración, Este comando eliminará la carpeta build de su proyecto. 

Para que funcione lo cambios en git tienen que ser trackeados y comentados sino te avisará.

## Leer mas

- [Documentación Create React App](https://facebook.github.io/create-react-app/docs/getting-started).

- [Documentación React](https://reactjs.org/).

### División de código 

- [https://es.reactjs.org/docs/code-splitting.html](https://es.reactjs.org/docs/code-splitting.html)

### Analizar el tamaño del paquete

- [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Hacer una aplicación web progresiva 

- [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Configuración avanzada

- [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

- [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)


### `yarn build` falla al minificar?

- [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)