import puppeteer from "puppeteer";

describe("App.js", () => {
  // --- global
  let browser;
  let page;

  beforeAll(async () => {
    browser = await puppeteer.launch();
    page = await browser.newPage();
  });

  // --- Tests end to end --- //

  it("Contiene el texto Hello World", async () => {
    await page.goto("http://localhost:5000");
    await page.waitForSelector(".header__title");
    const text = await page.$eval(".header__title", (e) => e.textContent);
    expect(text).toContain("Hello World");
  });

  it("Contiene el texto Made with 💗", async () => {
    await page.goto("http://localhost:5000");
    await page.waitForSelector(".footer > p");
    const text = await page.$eval(".footer > p", (e) => e.textContent);
    expect(text).toContain("Made with 💗");
  });

  // --- Tests end to end --- //

  afterAll(() => browser.close());
});
