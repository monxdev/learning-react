import React from "react";

// [ 1 ] import the React Test Renderer
import { create } from "react-test-renderer";

import Main from './index';

describe("Componente Main", () => {
  test("Incluye la clase section", () => {
    const root = create(<Main />).root;
    const element = root.findByType("main");
    expect(element.props.className.includes("main")).toBe(true);
  });
});