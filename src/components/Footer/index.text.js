import React from "react";

// [ 1 ] import the React Test Renderer
import { create } from "react-test-renderer";

import Footer from "./index";

describe("Componente Footer", () => {

  test("Incluye la clase footer", () => {
    const root = create(<Footer />).root;
    const element = root.findByType("footer");
    expect(element.props.className.includes("footer")).toEqual(true);
  });

  test("Incluye el texto 'hola que tal'", () => {
    const root = create(<Footer content="hola que tal" />).root;
    const element = root.findByType("footer");
    expect(element.props.children.props.children).toBe("hola que tal");
  });
});