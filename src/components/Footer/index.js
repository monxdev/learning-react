import React from "react";

// --- Import styles
import './style.scss';

const Footer = (props) => (
    <footer className="footer">
      <p>{props.content || 'Empty'} <a className="link" href="https://monchovarela.es">Moncho Varela</a></p>
    </footer>
  );


export default Footer;