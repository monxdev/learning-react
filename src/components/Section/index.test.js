import React from "react";

// [ 1 ] import the React Test Renderer
import { create } from "react-test-renderer";

import Section from './index';

describe("Componente Section", () => {
  test("Incluye la clase section", () => {
    const root = create(<Section content="hola" />).root;
    const element = root.findByType("section");
    expect(element.props.className.includes("section")).toBe(true);
  });
  test("Incluye el texto 'hola que tal'", () => {
    const root = create(<Section content="hola que tal" />).root;
    const element = root.findByType("section");
    expect(element.props.children.props.children).toBe('hola que tal')
  });
});