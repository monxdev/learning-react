import React from "react";

// --- Import styles
import './style.scss';

const Section = (props) => (
    <section className="section">
      <p>{props.content || 'Empty'}</p>
    </section>
  );


export default Section;
