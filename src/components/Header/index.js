import React, { useContext } from "react";

// --- Import styles
import './style.scss';

// --- Header component
const Header = (props) => {
  return (
    <header className="header">
      <h1 className="header__title">{props.title || 'Empty'}</h1>
    </header>
  );
};

// export Header 
export default Header;
