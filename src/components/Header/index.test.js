import React from "react";

// [ 1 ] import the React Test Renderer
import { create } from "react-test-renderer";

import Header from "./index";

describe("Componente Section", () => {
  test("Incluye la clase header", () => {
    const root = create(<Header />).root;
    const element = root.findByType("header");
    expect(element.props.className.includes("header")).toEqual(true);
  });

  test("Incluye el texto 'hola que tal'", () => {
    const root = create(<Header title="hola que tal" />).root;
    const element = root.findByType("header");
    expect(element.props.children.props.children).toBe("hola que tal");
  });
});
