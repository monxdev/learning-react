import React from 'react';
import { create } from "react-test-renderer";
import App from './App';

describe('Componente App', () => {

  test("Incluye el componente Header", () => {
    const root = create(<App/>);
    expect(root.toJSON().children[0].type).toBe("header");
  });

  test("Incluye el componente Section", () => {
    const root = create(<App/>);
    expect(root.toJSON().children[1].type).toBe("section");
  });

  test("Incluye el componente Footer", () => {
    const root = create(<App/>);
    expect(root.toJSON().children[2].type).toBe("footer");
  });
});