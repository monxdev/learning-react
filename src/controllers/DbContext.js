import React, { createContext } from "react";

// --- Create context
const DB = createContext();

export default DB;