import React, { useEffect, useState } from "react";

// --- controllers
import DB from './controllers/DbContext'

// --- Import styles
import "./App.scss";

// --- Import Json db
import Json from "./storage/db.json";

// --- Components
import Main from './components/Main/index';
import Header from './components/Header/index';
import Section from './components/Section/index';
import Footer from './components/Footer/index';

// --- App component
export default function App() {

  // --- initial state
  const [data, setData] = useState(Json);

  // --- Wait 2s and update states
  const updateHeaderTitle = (e) => {
    let newList = {...data};
    newList['title'] = 'Hello World 2';
    let w = setTimeout(() => {
      setData(newList);
      clearTimeout(w);
    },2000);
  };

  // --- On mount update header title
  useEffect(() => updateHeaderTitle(), []);

  return (
    <DB.Provider value={[data, setData]}>
      <Main>
        <Header title={data.title} />
        <Section content={data.content}/>
        <Footer content={data.footer}/>
      </Main>
    </DB.Provider>
  );
}
